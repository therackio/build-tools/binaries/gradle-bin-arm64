# gradle-bin-arm64

Gradle, compiled from source on ARM64.


NOTE: This is the binary only build (no docs or source) as described here: https://github.com/gradle/gradle/blob/master/CONTRIBUTING.md.


See https://gradle.org/ for more info.
